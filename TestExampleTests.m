//
//  TestExampleTests.m
//  TestExampleTests
//
//  Created by Vladimir Obrizan on 12/5/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "StudentService.h"

@interface TestExampleTests : XCTestCase

@end

@implementation TestExampleTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
    // 1. Initializations.
    
    
    // 2. Stimuli. (X)
    
    
    // 3. Check response. (Y)
    
    XCTAssert(2 + 2 == 4, @"Xcode doesn't work.");
    XCTAssertFalse(2 * 2 == 5, @"Xcode doesn't work.");
    
}


-(void)testCorrectInit {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[@3, @4, @5]];
    
    NSArray *a = @[@3, @4, @5];
    
    XCTAssertTrue([a isEqual:uut.marks]);
    
}


- (void)testIsValidMarks {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[@3, @4, @5]];
    
    XCTAssertTrue([uut isValidMarks]);
    
}

- (void)testIsValidMarks2 {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[@2, @4, @5]];
    
    XCTAssertFalse([uut isValidMarks]);
    
}

- (void)testIsNotValidMarks {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[@6, @4, @3]];
    
    XCTAssertFalse([uut isValidMarks]);
    
}

- (void)testIsNotValidMarks2 {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[@3, @4, @6]];
    
    XCTAssertFalse([uut isValidMarks]);
    
}


- (void)testIsValidMarks3 {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[]];
    
    XCTAssertTrue([uut isValidMarks]);
    
}



-(void)testIsPermittedToSession {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[@3, @3, @3, @3, @3]];
    
    XCTAssertTrue([uut isPermittedToSession]);
    
}


-(void)testIsNotPermittedToSession_no_marks {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[]];
    
    XCTAssertFalse([uut isPermittedToSession]);
    
}


-(void)testIsNotPermittedToSession_4_marks {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[@3, @3, @3, @3]];
    
    XCTAssertFalse([uut isPermittedToSession]);
    
}

-(void)testIsPermittedToSession_6_marks {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[@3, @3, @3, @3, @3, @5]];
    
    XCTAssertTrue([uut isPermittedToSession]);
    
}


-(void)testNoStipendia {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[@3, @3, @3, @3, @3]];
    
    XCTAssert([uut isStipendia] == NO);
}

-(void)testStipendia {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[@5, @5, @5, @4, @4]];
    
    XCTAssert([uut isStipendia] == YES);
    
}

-(void)testStipendia2 {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[@4, @4, @4, @4, @4]];
    
    XCTAssert([uut isStipendia] == YES);
    
}


-(void)testNoStipendia2 {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[@4, @4, @4, @4, @3]];
    
    XCTAssert([uut isStipendia] == NO);
    
}

-(void)testNoStipedia {
    
    StudentService *uut = [[StudentService alloc] initWithMarks:@[@5, @5, @5, @5]];
    
    XCTAssert([uut isStipendia] == NO);
    
}


-(void)testAverage {
    StudentService *uut = [[StudentService alloc] initWithMarks:@[]];
    
    NSArray *a = @[@2, @3];
    XCTAssert([uut average:a] == 2.5);
    
}




@end
