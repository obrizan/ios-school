//
//  StudentService.m
//  Laba5UnitTests
//
//  Created by Vladimir Obrizan on 19.12.13.
//  Copyright (c) 2013 Design and Test Lab. All rights reserved.
//

#import "StudentService.h"


@interface StudentService ()

@property (nonatomic, readwrite) NSArray *marks;

@end


//==============================================================================


@implementation StudentService


-(instancetype)initWithMarks:(NSArray *)marks
{
	self = [super init];
	if (self)
	{
		self.marks = marks;
	}
	return self;
}


//==============================================================================


-(BOOL)isValidMarks
{
	for (int i = 0; i < self.marks.count; i++)
	{
        NSInteger n = [self.marks[i] integerValue];
		if (n > 5 || n < 3)
			return NO;
	}
	
	return YES;
}


//==============================================================================


-(BOOL)isPermittedToSession
{
	return self.marks.count >= 5;
}


//==============================================================================


-(double)average:(NSArray *)a {
    double sum = 0;
    for (int i = 0; i < a.count; i++)
        sum += [a[i] doubleValue];
    return (sum / a.count);
}

-(BOOL)isStipendia
{
    if (![self isPermittedToSession])
        return NO;
    
	return [self average:self.marks] >= 4.0;
}


//==============================================================================

@end
